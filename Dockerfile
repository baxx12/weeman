FROM alpine:3.12

# This hack is widely applied to avoid python printing issues in docker containers.
# See: https://github.com/Docker-Hub-frolvlad/docker-alpine-python3/pull/13
ENV PYTHONUNBUFFERED=1

COPY . /app

WORKDIR /app

RUN apk add --no-cache python2 && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    pip install -r requirements.txt && \
    rm -r /root/.cache && \
    chmod -R 777 ./


CMD [ "python2", "weeman.py", "-d"]